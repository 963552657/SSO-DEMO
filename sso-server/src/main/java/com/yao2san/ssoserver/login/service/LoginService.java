package com.yao2san.ssoserver.login.service;

import javax.servlet.http.HttpSession;

/**
 * Created by wxg on 2019/5/4 19:34
 */
public interface LoginService {
    String getTicket(String userName);

    String createTicket(String uuid);

    boolean login(String userName,String password);

    boolean logout(String userName);
}
